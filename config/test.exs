use Mix.Config

# Configure your database
config :a_commerce_orders_services, ACommerceOrdersServices.Repo,
  username: "postgres",
  password: "postgres",
  database: "a_commerce_orders_services_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :a_commerce_orders_services, ACommerceOrdersServicesWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
