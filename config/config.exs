# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :a_commerce_orders_services,
  ecto_repos: [ACommerceOrdersServices.Repo]

# Configures the endpoint
config :a_commerce_orders_services, ACommerceOrdersServicesWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "A1DMaTZoEl7+j7BFeGN6M7el2rmLCqJTCH831FXeZVRq9+9fKzbbBIajEoR1NKTW",
  render_errors: [view: ACommerceOrdersServicesWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: ACommerceOrdersServices.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "DCN+Xv0v"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
