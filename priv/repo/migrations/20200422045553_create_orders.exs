defmodule ACommerceOrdersServices.Repo.Migrations.CreateOrders do
  use Ecto.Migration

  def change do
    create table(:orders) do
      add :user, :map
      add :products, {:array, :map}
      add :collect_time, :string
      add :price, :float
      add :order_id, :text
      add :collected, :boolean, default: false, null: false
      add :commerce_id, :integer
      
      timestamps()
    end

  end
end
