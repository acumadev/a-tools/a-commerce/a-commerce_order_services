defmodule ACommerceOrdersServices.CommerceTest do
  use ACommerceOrdersServices.DataCase

  alias ACommerceOrdersServices.Commerce

  describe "orders" do
    alias ACommerceOrdersServices.Commerce.Order

    @valid_attrs %{collect_time: "some collect_time", price: 120.5, products: [], user: "some user"}
    @update_attrs %{collect_time: "some updated collect_time", price: 456.7, products: [], user: "some updated user"}
    @invalid_attrs %{collect_time: nil, price: nil, products: nil, user: nil}

    def order_fixture(attrs \\ %{}) do
      {:ok, order} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Commerce.create_order()

      order
    end

    test "list_orders/0 returns all orders" do
      order = order_fixture()
      assert Commerce.list_orders() == [order]
    end

    test "get_order!/1 returns the order with given id" do
      order = order_fixture()
      assert Commerce.get_order!(order.id) == order
    end

    test "create_order/1 with valid data creates a order" do
      assert {:ok, %Order{} = order} = Commerce.create_order(@valid_attrs)
      assert order.collect_time == "some collect_time"
      assert order.price == 120.5
      assert order.products == []
      assert order.user == "some user"
    end

    test "create_order/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Commerce.create_order(@invalid_attrs)
    end

    test "update_order/2 with valid data updates the order" do
      order = order_fixture()
      assert {:ok, %Order{} = order} = Commerce.update_order(order, @update_attrs)
      assert order.collect_time == "some updated collect_time"
      assert order.price == 456.7
      assert order.products == []
      assert order.user == "some updated user"
    end

    test "update_order/2 with invalid data returns error changeset" do
      order = order_fixture()
      assert {:error, %Ecto.Changeset{}} = Commerce.update_order(order, @invalid_attrs)
      assert order == Commerce.get_order!(order.id)
    end

    test "delete_order/1 deletes the order" do
      order = order_fixture()
      assert {:ok, %Order{}} = Commerce.delete_order(order)
      assert_raise Ecto.NoResultsError, fn -> Commerce.get_order!(order.id) end
    end

    test "change_order/1 returns a order changeset" do
      order = order_fixture()
      assert %Ecto.Changeset{} = Commerce.change_order(order)
    end
  end
end
