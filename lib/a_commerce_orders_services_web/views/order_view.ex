defmodule ACommerceOrdersServicesWeb.OrderView do
  use ACommerceOrdersServicesWeb, :view
  alias ACommerceOrdersServicesWeb.OrderView

  def render("index.json", %{orders: orders}) do
    %{data: render_many(orders, OrderView, "order.json")}
  end

  def render("show.json", %{order: order}) do
    %{data: render_one(order, OrderView, "order.json")}
  end

  def render("order.json", %{order: order}) do
    %{id: order.id,
      user: order.user,
      products: order.products,
      collect_time: order.collect_time,
      price: order.price,
      collected: order.collected,
      order_id: order.order_id}
  end
end
