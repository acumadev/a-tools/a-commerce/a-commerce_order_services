defmodule ACommerceOrdersServicesWeb.OrderController do
  
  use ACommerceOrdersServicesWeb, :controller

  alias ACommerceOrdersServices.Commerce
  alias ACommerceOrdersServices.Commerce.Order

  action_fallback ACommerceOrdersServicesWeb.FallbackController

  def index(conn, %{"commerce_id" => commerce_id}) do
    orders = Commerce.list_orders(commerce_id)
    render(conn, "index.json", orders: orders)
  end
  
  def create(conn, %{"order" => order_params, "commerce_id" => commerce_id}) do
    order_params = order_params
    |> Map.put("commerce_id", commerce_id)
    |> Map.put("order_id", Ecto.UUID.generate())
    with {:ok, %Order{} = order} <- Commerce.create_order(order_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.order_path(conn, :show, commerce_id, order))
      |> render("show.json", order: order)
    end
  end

  def show(conn, %{"id" => id, "commerce_id" => commerce_id}) do
    order = Commerce.get_order!(id, commerce_id)
    render(conn, "show.json", order: order)
  end

  def update(conn, %{"id" => id, "order" => order_params, "commerce_id" => commerce_id}) do
    order = Commerce.get_order!(id, commerce_id)

    with {:ok, %Order{} = order} <- Commerce.update_order(order, order_params) do
      render(conn, "show.json", order: order)
    end
  end

  def delete(conn, %{"id" => id, "commerce_id" => commerce_id}) do
    order = Commerce.get_order!(id, commerce_id)

    with {:ok, %Order{}} <- Commerce.delete_order(order) do
      send_resp(conn, :no_content, "")
    end
  end
end
