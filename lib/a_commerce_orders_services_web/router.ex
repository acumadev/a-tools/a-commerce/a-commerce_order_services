defmodule ACommerceOrdersServicesWeb.Router do
  use ACommerceOrdersServicesWeb, :router

  pipeline :api do
    plug CORSPlug, origin: ["http://localhost:4200","http://54.200.130.180:4000","https://a-commerce.acuma.dev", "https://dashboard.a-commerce.acuma.dev"]
    plug :accepts, ["json"]
  end

  scope "/api", ACommerceOrdersServicesWeb do
    pipe_through :api

    resources "/:commerce_id/orders", OrderController, except: [:new, :edit]
    options   "/:commerce_id/orders/:id", OrderController, :options
    options   "/:commerce_id/orders", OrderController, :options
  end
end
