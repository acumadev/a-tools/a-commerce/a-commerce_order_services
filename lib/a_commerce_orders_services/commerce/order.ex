defmodule ACommerceOrdersServices.Commerce.Order do
  use Ecto.Schema
  import Ecto.Changeset

  schema "orders" do
    field :collect_time, :string
    field :price, :float
    field :products, {:array, :map}
    field :user, :map
    field :collected, :boolean, default: false
    field :order_id, :string
    field :commerce_id, :integer

    timestamps()
  end

  @doc false
  def changeset(order, attrs) do
    order
    |> cast(attrs, [:user, :products, :collect_time, :price, :collected, :order_id, :commerce_id])
    |> validate_required([:user, :products, :collected, :order_id, :commerce_id])
  end
end
