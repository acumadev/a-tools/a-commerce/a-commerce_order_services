defmodule ACommerceOrdersServices.Repo do
  use Ecto.Repo,
    otp_app: :a_commerce_orders_services,
    adapter: Ecto.Adapters.Postgres
end
